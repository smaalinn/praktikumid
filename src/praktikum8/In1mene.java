package praktikum8;

import lib.TextIO;

public class In1mene {
	
	String nimi;
	int vanus;
	
	public In1mene(String nimi, int vanus) {
		this.nimi = nimi;
		this.vanus = vanus;
	}
	
	public boolean equals(In1mene teine) {
		return teine.vanus == this.vanus
		  && this.nimi.equals(teine.nimi);
	}
	
	public void tervita() {
		TextIO.putln("Tere, minu nimi on " + nimi + ", olen " + vanus + "-aastane.");
	}
	
	@Override
	public String toString() {
		return nimi + " " + vanus;
	}
}