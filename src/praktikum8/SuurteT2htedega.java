package praktikum8;

public class SuurteT2htedega {

	public static void main(String[] args) {
		System.out.print("Kirjuta sõna:");

		String sona = lib.TextIO.getlnString();
		{

			for (int i = 0; i < sona.length(); i++) {
				System.out.printf("%s", sona.toUpperCase().charAt(i));
				if (i != sona.length() - 1) {
					System.out.print("-");
				}
			}
		}
	}
}
