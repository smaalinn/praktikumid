package praktikum8;

import lib.TextIO;

public class Igatpidi {

	public static void main(String[] args) {
		System.out.println("Sisesta sõna");
		
		String s6na = TextIO.getlnString();
		String tagurpidi = new StringBuilder(s6na).reverse().toString();
		
		if (s6na.equals(tagurpidi)) {
			System.out.println("Sõna on palindroom.");
		} else {
			System.out.println("Sõna ei ole palindroom.");
		}
	}

}