package praktikum5;

public class Kulljakiri {

	public static void main(String[] args) {

		while (true) {
			int arvutiMyndiVise = Meetodid.suvalineArv(0, 1);
			System.out.println("Arvuti arvas: " + arvutiMyndiVise);
			int kasutajaArvamus = Meetodid.kasutajaSisestus("Sisesta kull (0) või kiri (1)", 0, 1);

			if (kasutajaArvamus == arvutiMyndiVise) {
				System.out.println("Sina võitsid!");
			} else {
				System.out.println("Sa kaotasid!");
			}
		}

	}
}