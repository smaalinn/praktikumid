package praktikum5;

import praktikum1.TextIO;

public class Vahemik {

	public static void main(String[] args) {

		int kasutajaSisestus = kasutajaSisestus(1, 10);
		System.out.println("kasutajaSisestus meetod tagastas: " + kasutajaSisestus);
	}

	public static int kasutajaSisestus(int min, int max) {

		System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
		int sisestus = TextIO.getlnInt();
		if (sisestus >= min && sisestus <= max) {
			return sisestus;
		} else {
			System.out.println("Vigane sisestus! Proovi uuesti.");
			return 0;
		}

	}

}
