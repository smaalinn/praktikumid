package praktikum5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ArvudeVahemik {

	public static void main(String[] args) {
		int kasutajaArv;
		kasutajaArv = kasutajaSisestus(1, 10);
		System.out.println("Sisestatud arv oli " + kasutajaArv);
	}

	public static int kasutajaSisestus(int _min, int _max) {
		int kasutajaArv = 0;
		// loon objekti Scanner
		Scanner stdin = new Scanner(System.in);

		while (true) {
			System.out.println("Sisesta arv vahemikus " + _min + " ja " + _max);

			// try/catch väldib programmi kokkujooksmist kui sisestada numbri
			// asemel nt täht
			try {
				kasutajaArv = stdin.nextInt();
			} catch (InputMismatchException e) {
				System.out.println(e);
				break;
			}

			// kontrollin sisestatud arvu vastavust tingimustele
			if (_min <= kasutajaArv && kasutajaArv <= _max) {
				break;
			} else {
				System.out.println("Arv ei j22 antud vahemikku. proovi uuesti");
			}
		}

		// sulgen scanneri
		stdin.close();

		return kasutajaArv;
	}
}