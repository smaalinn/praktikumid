package praktikum5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Liisk {
	public static void main(String[] args) {
		int inimesteArv = 0;
		int juhuArv = 0;
		Scanner stdin = new Scanner(System.in);

		System.out.print("Sisesta inimeste arv: ");

		try {
			inimesteArv = stdin.nextInt();
		} catch (InputMismatchException e) {
			System.out.println(e);
		}
		// saan juhuarvu kasutades klassis "KullKiri" asuvat meetodit "juhuArv"
		juhuArv = KullKiri.juhuArv(1, inimesteArv);

		System.out.println("Valitud sai " + juhuArv + ". inimene");
		stdin.close();
	}

}