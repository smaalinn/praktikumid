package praktikum5;

public class KullKiri {
	public static void main(String[] args) {
		int sisestatudArv = 0;
		int arvutiArv = juhuArv(1, 2);
		System.out.println("Arva kas tuleb kull v6i kiri! Sisesta 1 või 2");
		sisestatudArv = ArvudeVahemik.kasutajaSisestus(1, 2);

		switch (sisestatudArv) {
		case 1:
			System.out.println("Valisid \"kull\"");
			break;
		case 2:
			System.out.println("Valisid \"kiri\"");
			break;
		}

		switch (arvutiArv) {
		case 1:
			System.out.println("Arvuti valis \"kull\"");
			break;
		case 2:
			System.out.println("Arvuti valis  \"kiri\"");
			break;
		}

		if (arvutiArv == sisestatudArv) {
			System.out.println("Sina võitsid");
		} else {
			System.out.println("Sina kaotasid");
		}

	}

	public static int juhuArv(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));
	}

}