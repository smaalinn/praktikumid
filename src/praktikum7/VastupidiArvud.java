package praktikum7;

import lib.TextIO;

public class VastupidiArvud {

	public static void main(String[] args) {
		
		int[] m = new int[10];
		int i;
		
		System.out.println("Sisesta kümme suvalist arvu");
		for (i = 0; i < m.length; i = i + 1) {
			m[i] = TextIO.getlnInt();
		}
		System.out.println("Sisestatud numbrid tagurpidi");
		for (i = m.length - 1; i > 0; i = i - 1) {
			System.out.println(m[i]);
		}
		
	} 
	
}
