package praktikum15;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GraafikaNaide extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Kood on pärit http://docs.oracle.com/javafx/2/canvas/jfxpub-canvas.htm
	 */
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistamise näide");
		Group root = new Group();
		Canvas canvas = new Canvas(400, 400);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void joonista(GraphicsContext gc) {
		
//		Lumehelves l = new Lumehelves(100, 200);
//		l.joonistaMind(gc);
//		
//		Lumehelves l2 = new Lumehelves(50, 100);
//		l2.joonistaMind(gc);
		
		new GraafilineLumesadu(20, gc);
		
	}
}