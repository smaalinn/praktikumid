package harjutus;

//Koostage Java-meetod, mis leiab etteantud massiivi m elementide ruutude summa.
//Write a Java method to find the sum of squares of elements of a given array m.
//public static int ruutudeSumma (int[] m)

public class RuuduSumma {

	public static void main(String[] args) {

		int[] m = { 0, 3, 3 };
		int summa = ruutudeSumma(m);
		System.out.println(summa);
	}

	public static int ruutudeSumma(int[] m) {

		int summa = 0, temp;
		for (int loendaja = 0; loendaja < m.length; loendaja++) {
			temp = m[loendaja];
			summa = summa + (temp * temp);
		}
		return summa;
	}
}