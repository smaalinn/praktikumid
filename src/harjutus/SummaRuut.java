package harjutus;

//Koostage Java-meetod, mis leiab kahe etteantud reaalarvu summa ruudu.
//Write a method in Java to find the square of the sum of two given real
//numbers. public static double summaRuut (double a, double b)

public class SummaRuut {

	public static void main(String[] args) {

		double a, b, test;
		a = 2;
		b = 3;
		test = summaRuut(a, b);
		System.out.println(test);
	}

	public static double summaRuut(double a, double b) {

		double summaRuut = 0;
		double summa = a + b;
		summaRuut = summa * summa;

		return summaRuut;
	}
}