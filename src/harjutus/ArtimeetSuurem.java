package harjutus;

//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d p6hjal niisuguste elementide arvu, mis on 
//rangelt suuremad k6igi elementide aritmeetilisest keskmisest (aritmeetiline keskmine = summa / elementide_arv).
//Write a method in Java to find the number of elements strictly greater than arithmetic mean of all elements of a 
//Given array of real numbers d (arithmetic mean = sum_of_elements / number_of_elements).
//public static int keskmisestParemaid (double[] d)

public class ArtimeetSuurem {

	public static void main(String[] args) {

		double[] d = { 2, 3, 4, 4.1 };
		int vastus = keskmisestParemaid(d);
		System.out.println(vastus);
	}

	public static int keskmisestParemaid(double[] d) {

		int vastus = 0;
		double summa, keskmine;
		summa = 0;

		for (int loendaja = 0; loendaja < d.length; loendaja++) {
			summa = d[loendaja] + summa;
		}
		keskmine = summa / d.length;
		for (int loendaja = 0; loendaja < d.length; loendaja++) {
			if (d[loendaja] > keskmine) {
				vastus++;
			}
		}
		return vastus;
	}
}