package harjutus;

//Koostage Java-meetod, mis teeb kindlaks, kas etteantud t2isarv n on
//negatiivne paaritu arv. Write a method in Java to check whether a given
//integer n is negative odd number. public static boolean negPaaritu (int
//n)

public class NegatiivneArv {

	public static void main(String[] args) {

		int n;
		boolean test;
		n = -3;
		test = negPaaritu(n);
		System.out.println(test);
	}

	public static boolean negPaaritu(int n) {

		boolean vastus;
		if (n < 0 && 0 != n % 2) {
			vastus = true;
		} else {
			vastus = false;
		}
		return vastus;
	}
}