package harjutus;

//Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal 
//niisuguste elementide arvu, mis on rangelt väiksemad kõigi elementide 
//aritmeetilisest keskmisest (aritmeetiline keskmine = summa / elementide_arv). 
//Write a method in Java to find the number of elements strictly less than 
//arithmetic mean of all elements of a given array of real numbers d 
//(arithmetic mean = sum_of_elements / number_of_elements).
//    public static int allaKeskmise (double[] d)

public class AritmeetV2iksem {

	public static void main(String[] args) {

		double[] massiiv = { 1, 5, 7, 3, 8, 9, 4 };
		int count = allaKeskmise(massiiv);
		System.out.println(count);
	}

	public static int allaKeskmise(double[] d) {

		int belowAvCount = 0;
		double average = 0;
		double sum = 0;

		for (double el : d) {
			sum += el;
		}
		average = sum / d.length;

		for (double el : d) {
			if (el < average) {
				belowAvCount++;
			}
		}
		return belowAvCount;
	}
}



//public class Answer {
//
//	   public static void main (String[] args) {
//	      System.out.println (allaKeskmise (new double[]{1,10, 1}));
//	      // YOUR TESTS HERE
//	   }
//
//	   public static int allaKeskmise (double[] d) {
//		   
//		   //leiame summa
//		   double summa = 0;
//		   for(int i = 0; i < d.length ; i++){
//			   summa += d[i];
//		   }
//		   
//		   //leiame keskmise
//		   double keskmine = summa / d.length;
//		   
//		   int vastus = 0;
//		   
//		   for(int i = 0; i < d.length ; i ++){
//			   if(d[i] < keskmine){
//				   vastus++;
//			   }
//		   }
//		   
//	      return vastus;  // YOUR PROGRAM HERE
//	   }
//
//	}