package harjutus;

//Koostada Java meetod, mis asendab parameetrina 
//etteantud sõnes s kõik suurtähed märgiga '_'.
//Write a Java method to replace all uppercase 
//letters in a given string s by symbol '_'.
//  public static String asenda (String s)

public class AllkriipsAsendus {

	public static void main(String[] args) {

		String s = "Tere, TUDENG!";
		String t = asenda(s); // "_ere, ______!"
		System.out.println(t);
	}

	public static String asenda(String s) {

		StringBuffer a = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			if (Character.isUpperCase(s.charAt(i))) {
				a.append('_');
			} else {
				a.append(s.charAt(i));
			}
		}
		s = a.toString();
		return s;
	}
}