package harjutus;

//Sportlase punktisumma arvutatakse üksikkatsetest saadud punktide summana, millest on maha võetud kahe halvima katse tulemused(üksikkatseid on rohkem kui kaks). 
//Kirjutada Java meetod, mis arvutab punktisumma üksikkatsete tulemuste massiivi põhjal. Parameetriks olevat massiivi muuta ei tohi.
//Sportsmans score is calculated as sum of points from different attempts and two worst attempts are not counted (there are more than two attempts).
//Write a Java method to calculate the score if an array of points from all attempts is given. Do not change the parameter array.

public class SportlasePunktid {

	public static void main(String[] args) {

		System.out.println(score(new int[] { 4, 1, 2, 3, 0 })); // 9
	}

	public static int score(int[] points) {

		int vastus = 0, min1 = points[0], min2 = points[1], temp = 0;
		for (int x = 0; x < points.length; x++) {
			if (min1 > points[x]) {
				min1 = points[x];
				temp = x;
			}
		}
		for (int x = 0; x < points.length; x++) {
			if (min2 > points[x] && x != temp) {
				min2 = points[x];
			}
		}

		for (int x = 0; x < points.length; x++) {
			vastus = points[x] + vastus;
		}
		vastus = vastus - min1 - min2;

		return vastus;
	}

}