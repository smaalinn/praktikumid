package harjutus;

//Koostage Java-meetod, mis leiab etteantud massiivi m rangelt 
//positiivsete elementide arvu. Write a method in Java to find the 
//number of strictly positive elements of a given array m.
//public static int posElArv (int[] m)

public class MituPosArvu {

	public static void main(String[] args) {

		System.out.println(posElArv(new int[] { -1, 0, 1 }));
	}

	public static int posElArv(int[] m) {

		int n = 0;
		for (int i = 0; i < m.length; i++) {
			if (m[i] > 0) {
				n++;
			}
		}
		return n;
	}
}