package harjutus;

//Koostage Java meetod etteantud täisarvumaatriksi m reamiinimumide massiivi leidmiseks 
//(massiivi i-s element on maatriksi i-nda rea vähima elemendi väärtus). Read võivad olla
//erineva pikkusega.
//Write a method in Java to find the array of minimums of rows of a given matrix of
//integers m (i-th element of the answer is the minimum of elements of the i-th row in 
//the matrix). Rows might be of different length.
//
//public static int[] reaMinid (int[][] m)

public class ReaMin {

	public static void main(String[] args) {

		int[] res = reaMinid(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } });
	}

	public static int[] reaMinid(int[][] m) {

		int[] miinimumid = new int[m.length];

		for (int i = 0; i < m.length; i++) {
			int min = 0;
			for (int j = 0; j < m[i].length; j++) {

				if (j == 0) {
					min = m[i][j];
				} else {
					if (min > m[i][j]) {
						min = m[i][j];
					}
				}
			}
			miinimumid[i] = min;
		}
		for (int i = 0; i < miinimumid.length; i++) {
			System.out.println(miinimumid[i]);
		}
		return miinimumid;
	}
}
