package harjutus;

//Koostage Java meetod etteantud täisarvumaatriksi m veerusummade massiivi leidmiseks (massiivi j-s element on maatriksi j-nda veeru summa).
//Arvestage, et m read võivad olla erineva pikkusega.
//Write a method in Java to find the array of sums of columns of a given matrix of integers m (j-th element of the answer is the sum of elements of the j-th column in the matrix).
//Rows of m might be of different length.

public class VeeruSummadX {

	public static void main(String[] args) {

		int[] res = veeruSummad(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } });
	}

	public static int[] veeruSummad(int[][] m) {

		int[] vastus = new int[m.length];
		for (int x = 0; x < m.length; x++) {
			vastus[x] = 0;
			for (int y = 0; y < m[x].length; y++) {
				vastus[x] = vastus[x] + m[x][y];
			}
		}
		return vastus;
	}
}


//public class VeeruSummadX {
//    /*
//     * Koostage Java meetod etteantud tהisarvumaatriksi m veerusummade massiivi
//     * leidmiseks (massiivi j-s element on maatriksi j-nda veeru summa).
//     * Arvestage, et m read vץivad olla erineva pikkusega.
//     */
//
//    public static void main(String[] args) {
//            int[] res = veeruSumma(new int[][] { { 1, 2, 3, 4, 5 }, { 4, 5, 6 },
//                            { 8, }, { 8, 2, 7, 1 }, { 8, 3, 4, 5, 2, 1 }
//
//            });
//            // YOUR TESTS HERE
//    }
//
//    public static int[] veeruSumma(int[][] m) {
//            // Maksimaalne rea pikkus
//            int maxrida = 0;
//            for (int x = 0; x < m.length; x++)
//                    if (maxrida < m[x].length)
//                            maxrida = m[x].length;
//
//            int[] summa = new int[maxrida];
//
//            for (int x = 0; x < summa.length; x++)
//                    summa[x] = 0; // sellest enam suuremaks ei saa :D
//
//            for (int i = 0; i < m.length; i++) {
//                    for (int j = 0; j < m[i].length; j++) {
//                            // if(min[j]>m[i][j])
//                            summa[j] = summa[j] + m[i][j];
//                    }
//            }
//            // ****ainult testimise jaoks*******
//            for (int z = 0; z < summa.length; z++)
//                    System.out.print(summa[z] + " , ");
//            // ***********************************
//            // TODO!!! YOUR PROGRAM HERE
//            return summa;
//    }
//
//}