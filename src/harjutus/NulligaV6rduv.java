package harjutus;

//Koostage Java-meetod, mis leiab etteantud täisarvude massiivi m nulliga võrduvate elementide arvu. 
//Write a method in Java to find the number of zeros in a given array of integers m.
//   public static int nullideArv (int[] m)

public class NulligaV6rduv {

	public static void main(String[] args) {

		System.out.println(nullideArv(new int[] { 0, 1 }));
	}

	public static int nullideArv(int[] m) {

		int temp = 0;
		for (int i = 0; i < m.length; i++) {
			if (m[i] == 0) {
				temp++;
			}
		}
		return temp;
	}
}