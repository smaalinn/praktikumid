package harjutus;

//Koostage Java meetod etteantud täisarvumaatriksi m reamaksimumide 
//massiivi leidmiseks (massiivi i-s element on maatriksi i-nda rea 
//suurima elemendi väärtus). Read võivad olla erineva pikkusega. 
//Write a method in Java to find the array of maximums of rows of 
//a given matrix of integers m (i-th element of the answer is the
//maximum of elements of the i-th row in the matrix). 
//Rows might be of different length.
//   public static int[] reaMaxid (int[][] m)

public class ReaMax {

	public static void main(String[] args) {

		int[] res = reaMaxid(new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { -3 } }); // {3,
	}

	public static int[] reaMaxid(int[][] m) {

		int[] vastus = new int[m.length];

		for (int l = 0; l < m.length; l++) {

			vastus[l] = m[l][0];
			for (int i = 0; i < m[l].length; i++) {
				if (vastus[l] < m[l][i]) {
					vastus[l] = m[l][i];
				}
			}
		}
		return vastus;
	}
}



//public class yl5 {
//	 
//	   public static void main (String[] args) {
//	      int[] res = reaMaxid (new int[][] { {1,2,0}, {3,6,0} }); // {3, 6}
//	      System.out.print(Arrays.toString(res));
//	      // YOUR TESTS HERE
//	   }
//	 
//	   public static int[] reaMaxid (int[][] m)
//	   {
//	           int massiiv[] = new int[m.length];
//	           
//	           for ( int i = 0; i<m.length; i++)
//	           {
//	                   for ( int j = 0; j<m[i].length; j++)
//	                   {
//	                           int suurim = m[i][0];
//	                           
//	                           if ( m[i][j] > suurim )
//	                           {
//	                                   suurim = m[i][j];
//	                           }
//	                        massiiv[i] = suurim;  
//	                   }
//	                   
//	           }
//	           return massiiv;
//	           
//	   }



//public class ReaMax{
//
//	   public static void main (String[] args) {
//	      int[] res = reaMaxid (new int[][] { {1,2,0}, {3,6,0} }); // {3, 6}
//	      System.out.print(Arrays.toString(res));
//	      // YOUR TESTS HERE
//	   }
//
//	   public static int[] reaMaxid (int[][] m)
//	   {
//		   int massiiv[] = new int[m.length];
//		   
//		   for ( int i = 0; i<m.length; i++)
//		   {
//			   for ( int j = 0; j<m[i].length; j++)
//			   {
//				   int suurim = m[i][0];
//				   
//				   if ( m[i][j] > suurim )
//				   {
//					   suurim = m[i][j];
//				   }
//				massiiv[i] = suurim;   
//			   }   
//		   }
//		   return massiiv;
//	   }

