package harjutus;

// Koostage Java meetod, mis genereerib parameetrina etteantud n järgi niisuguse
// n korda n täisarvumaatriksi, mille iga elemendi väärtuseks on selle elemendi
// reaindeksi ja veeruindeksi summa ruut (indeksid algavad nullist).
// Write a method in Java to generate an integer matrix of size n x n (n is a
// parameter of the method) elements of which are calculated by finding the
// square of the sum of the row index and the column index of the element
// (indices start from zero).
// public static int[][] muster (int n)

public class ParameeterNX {
	
    public static void main (String[] args) {
    	
        int[][] res = muster (9);

        for (int[] row : res){
            for(int col : row){
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }

    public static int[][] muster (int n) {
    	
        int[][] massiiv = new int[n][n];
        for(int i = 0; i < massiiv.length; i++){
            for(int j = 0; j < massiiv[i].length; j++){
                if(i > j){
                    massiiv[i][j] = i;
                }else{
                    massiiv[i][j] = j;
                }
            }
        }


        return massiiv;
    }
}




//public class ParameeterN{
//	public static void main(String[] args) {
//		int[][] res = muster(9);
//
//		for (int i = 0; i < 9; i++) {
//			for (int j = 0; j < 9; j++) {
//				System.out.print(res[i][j]);
//			} // sisemine
//		} // v6limine
//	} // main
//
//	public static int[][] muster(int n) {
//		int[][] ruudud = new int[n][n];
//
//		for (int x = 0; x < n; x++) {
//			for (int y = 0; y < n; y++) {
//				ruudud[x][y] = (int) Math.pow((y + x), 2);
//			} // v6limine
//		} // sisemine
//		return ruudud;
//	}
//}