package praktikum13;

import java.util.ArrayList;

public class MaatriksiLugeja {

	public static void main(String[] args) {
		
		//double[][]
		//ArrayList<ArrayList<Doubles>>
		//ArrayList<Double[]>
		
		
		String kataloogitee = MaatriksiLugeja.class.getResource(".").getPath();
		ArrayList<String> failiSisu = FailiLugeja.loeFail(kataloogitee + "maatriks.txt");
		System.out.println(failiSisu);
		
		ArrayList<ArrayList<Double>> maatriks = new ArrayList<ArrayList<Double>>();
		for (String read : failiSisu) {
			String[] elemendid = read.split(" ");
			ArrayList<Double> maatriksiRida = new ArrayList<Double>();
			for (String el : elemendid) {
				double nr = Double.parseDouble(el);
				maatriksiRida.add(nr);
				
			}
			maatriks.add(maatriksiRida);
		} System.out.println(maatriks);
	}

}
