package praktikum9;

import lib.TextIO;

public class Rekursiiv {

	public static void main(String[] args) {


		
		System.out.println("Sisesta arv mida on vaja astendada");
		int arv = TextIO.getlnInt();
		System.out.println("Sisesta astendaja");
		int aste = TextIO.getlnInt();
		
		int astenda = (int) Math.pow(arv, aste);
		System.out.println("Vastus on " + astenda);
	}
	}
