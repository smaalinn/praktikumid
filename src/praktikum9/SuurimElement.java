package praktikum9;

public class SuurimElement {

	public static void main(String[] args) {
		int[] massiiv = { 1, 3, 6, 7, 8, 3, 5, 7, 21, 3 };
		System.out.println(maxValue(massiiv));
	}

	public static int maxValue(int[] massiiv) {
		int max = Integer.MIN_VALUE;
		for (int el : massiiv) {
			if (el > max) {
				max = el;
			}
		}
		return max;
	}
}


// public static int suurim(int[] sisend) {
//	
//	int seniSuurim = 0;
//	for (int i : sisend) {
//		if (i > seniSuurim)
//			seniSuurim = i;
//	}
//	return seniSuurim;
//}
//}
