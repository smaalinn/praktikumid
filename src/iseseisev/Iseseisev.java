package iseseisev;

import java.util.Scanner;

import lib.TextIO;

public class Iseseisev {

	public static void main(String[] args) {

		System.out.println("Palun sisestage soovitud teema number.");
		System.out.println("1. Liitmine, lahutamine\n2. Astendamine");
		System.out.println("3. Astendamine\n4. Ruutvõrrand.\n5. Kolmnurk");
		int soov = TextIO.getlnInt();

		if (soov == 1) {
			while (true) {
				System.out.println(
						"Liitmine ja lahutamine.\nSisestage arvud ükshaaval.\nVastuseks saamiseks kirjutage viimaseks arvuks 0.\nLahutamise korral lisage numbri ette miinus märk.");
				Scanner lugeja = new Scanner(System.in);
				int tulemus = 0;
				while (true) {
					int arv = Integer.parseInt(lugeja.nextLine());
					if (arv == 0) {
						break;
					}
					tulemus += arv;
				}
				System.out.println("Vastuseks on " + tulemus + ".");
			}
		}
		if (soov == 2) {
			while (true) {
				System.out.println("Astendamine.\nSisesta arv mida on vaja astendada.");
				int arv = TextIO.getlnInt();
				System.out.println("Sisesta astendaja.");
				int aste = TextIO.getlnInt();

				int astenda = (int) Math.pow(arv, aste);
				System.out.println("Vastus on " + astenda + ".");
			}
		}
		if (soov == 3) {
			while (true) {
				System.out.println(
						"Korrutamine.\nSisestage arvud ükshaaval.\nVastuseks saamiseks kirjutage viimaseks arvuks 0.");
				Scanner lugeja = new Scanner(System.in);
				int tulemus = 1;
				while (true) {
					int arv = Integer.parseInt(lugeja.nextLine());
					if (arv == 0) {
						break;
					}
					tulemus *= arv;
				}
				System.out.println("Vastuseks on " + tulemus);
			}
		}
		if (soov == 4) {
			while (true) {
				System.out.println(
						"Ruutvõrrand.\nVõrrand on kujul ax^2 + bx + c.\nNegatiivse puhul lisage miinusmärk järgneva arvu ette.");
				System.out.println("Sisestage a.");
				int a = TextIO.getlnInt();
				System.out.println("Sisestage b.");
				int b = TextIO.getlnInt();
				System.out.println("Sisestage c.");
				int c = TextIO.getlnInt();
				if (Math.pow(b, 2) < (4 * a * c)) {
					System.out.println("x1 ja x2 puuduvad.");
				} else {
					double x1 = (-b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
					double x2 = (-b - Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
					System.out.println("x1 on " + x1 + " ja x2 on " + x2 + ".");
				}
			}
		}
		if (soov == 5) {
			System.out.println("1. Ümbermõõt\n2. Pindala");
			int valik = TextIO.getlnInt();
			if (valik == 1) {
				System.out.println("Sisestage esimene külg.");
				int a = TextIO.getlnInt();
				System.out.println("Sisestage teine külg.");
				int b = TextIO.getlnInt();
				System.out.println("Sisestage kolmas külg.");
				int c = TextIO.getlnInt();
				int umbermoot = a + b + c;
				System.out.println("Kolmnurga ümbermõõt on " + umbermoot + " pindala ühikut");
			}
		}
	}
}

//Programmeerimiskeele süntaksit kirjeldab...
//b. grammatika
//
//Milline alljärgnevatest keeltest esindab objektorienteeritud programmeerimise paradigmat?
//d. Smalltalk
//
//Milline alljärgnevatest keeltest esindab imperatiivse programmeerimise paradigmat?
//a. Ada
//
//Milline alljärgnevatest keeltest esindab loogilise programmeerimise paradigmat?
//d. Prolog
//
//Milline järgnevatest tüüpidest EI OLE algtüüp (primitive type)?
//c. Long
//
//Milline järgnevatest keele Java tüüpidest on algtüüp (primitive type)?
//b. boolean
//
//Mida tähistab keeles Java järgmine kirjutis? -0xa
//c. 32-bitine täisarv -10
//
//Mida tähistab keeles Java järgmine kirjutis? -0.5e-2
//d. 64-bitine reaalarv -0.005
//
//Mida tähistab keeles Java järgmine kirjutis? -0.5e-2f 
//b. 32-bitine reaalarv -0.005
//
//Millise tulemuse annab järgmine programmilõik? 
//int a = 9; 
//int b = ++a / 5; 
//d. a==10 && b==2
//
//Millise tulemuse annab järgmine programmilõik? 
//[code java]
//int a = 4;
//int b = a++ / 5;
//[/code]
//d. a==5 && b==0
//
//Mida väljendab alljärgneva programmilõigu teine lause? 
//String s = "Hello"; 
//int len = s.length(); 
//a. klassi String klassimeetodi poole pöördumist
//
//Milline tingimus kirjeldab n mittekuulumist poollõiku [0, 10) ? 
//b. (n <0) || (n >= 10)
//
//Võtmesõna "while" järele ümarsulgudesse kirjutatav tingimus on... 
//d. tsükli jätkamise tingimus
//
//Käsu return järele kirjutatud avaldisega määratakse: 
//d. tagastusväärtus d
//
//Java return lause...
//a. annab juhtimise tagasi meetodi väljakutsujale
//
//Meetodi nimi koos piiritlejate, parameetrite tüüpide ning tagastusväärtuse tüübiga on tuntud kui meetodi:
//b. signatuur
//
//Java rakendus (application) peab sisaldama meetodit, mille signatuur on...
//d. public static void main (String[] )
//
//Millist tüüpi väärtuse tagastab meetod m?
//public static int m (double d, String s);
//a. int
//
//Javadoc @version abil dokumenteeritakse: 
//a. klassi versiooniinfot
//
//Javadoc @since abil dokumenteeritakse:
//d. varaseimat Java kompilaatori versiooni, millega klass kompileerub
//
//Javadoc @param abil dokumenteeritakse:
//a.meetodi parameetrit
//
//Mida väljendab alljärgnev programmilõik?
//objekti s isendi poole pöördumist
//
//Millist tüüpi väärtuse tagastab meetod m?
//public static String m (double d, int i);
//c. String
//
//Javadoc @return abil dokumenteeritakse: 
//b. meetodi tagastusväärtust
//
//Java koodistiil nõuab konstantide nimede kirjutamist:
//d. väikese algustähega
//
//Java koodistiil nõuab klassinimede kirjutamist:
//a.suure algustähega
//
//Java koodistiil nõuab muutujate nimede kirjutamist:
//a.suure algustähega
//
//Teate saatmine objektile tähendab:
//b. pöördumist vastava isendimeetodi poole
//
//Objekti (isendi) identiteedi Javas määrab:
//a. mäluaadress
//
//Destruktor on:
//b. meetod isendi hävitamiseks, keeles Java puudub
//
//Konstruktor on meetod isendi loomiseks
//
//Liides List keeles Java võimaldab: 
//d. salvestada andmestruktuuri korduvaid elemente, mida eristatakse indeksi järgi
//
//Liides Comparable keeles Java võimaldab:
//c. otsustada, kumb kahest elemendist on suurem
//
//Liides Map keeles Java võimaldab:
//a. salvestada paare "võti-väärtus"
//
//Java break lause... 
//d. lõpetab sisemise tsükli või valikulause täitmise
//
//Paaride "võti - väärtus" salvestamiseks sobib Java APIs klass: 
//c. HashMap 
//
//Paisktabelit esindab Java APIs liides:
//b. Map
//
//Millise eriolukorra (loetletutest) tuvastamine ja töötlemine on Javas mõttekas: 
//b. Exception
//
//Eriolukorda, mille töötlemine programmeerija poolt on Javas kohustuslik, nimetatakse:
//a. checked exception
//
//Eriolukorda, mille töötlemine programmeerija poolt ei ole Javas kohustuslik, nimetatakse:
//b. RuntimeException 
//
//Baidivoo lugemisel meetodiga read() annab voo lõppemisest märku:
//b. (int)-1 
//
//Tekstivoo lugemisel meetodiga read() annab voo lõppemisest märku:
//a. (String)null
//
//Millist kasutajaliidese aspekti juhib paigutushaldur?
//a. liidese visuaalne kuju
//
//Ühene pärimine tähendab, et: 
//d. iga klass saab olla ülimalt ühe klassi alamklass
//
//Mitmene pärimine tähendab, et:
//b. klass võib omadusi pärida mitmelt ülemklassilt
//
//Rakendi kuva värskendamist saab tellida pöördudes meetodi ... poole.
//b. repaint
//
//Rakendi passiivseks muutumisel pöördutakse meetodi ... poole. 
//a. stop
//
//Rakendi elutsükli alguses täidetakse meetod:
//b. init
//
//Milline käsk joonistab ringi (pinna) läbimõõduga 50 pikselit?
//d. g.fillOval (50, 50, 50, 50);
//
//Milline käsk joonistab ringjoone läbimõõduga 50 pikselit? 
//d. g.drawOval (50, 50, 50, 50);
//
//Milline käsk joonistab ringjoone raadiusega 50 pikselit?
//c. g.drawOval (0, 0, 100, 100);
//
//Java algtüüpidesse kuuluvate andmete sisestamiseks on ette nähtud meetodid klassis: 
//a. DataInputStream 
//
//Java algtüüpidesse kuuluvate andmete väljastamimseks on ette nähtud meetodid klassis:
//d. DataOutputStream d
//
//Millist kasutajaliidese aspekti juhivad kuularid? 
//a.liidese reageerimine sündmustele
//
//Java programmi lähtekood on failis laiendiga? 
//.java
//
//Olgu muutuja m kelles Java kirjeldatud kui täisarvude massiiv. Mälu eraldamiseks massiivi m 10 elemendi jaoks tuleb kirjutada:
//a. m=new int[10]
//
//Java continue lause 
//lõpetab sisemise tsükli sammu
//
//Java programmi transleerimisel tekkiv baitkood on failis laiendiga 
//.class
//
//Kahe elemendi omavahelise järjestuse määramiseks sobib java APIs liides:
//b)Comparable
//
//Meetodi päises kkirjutatud muutujad on
//b. tegelikud väärtused
//
//iteraator võimaldab:
//d. küsida andmekogumi järgmist elementi
//
//Korduvate elementideta järjestatud hulka esindab java APIs klass:
//a. ArrayList
//
//Isendimuutujate väärtused määravad objekti:
//Values of instance variables determine:
//1. oleku; object state
//
//Hiline seostamine tähendab,et
//d.teate poolt aktiviseeritav meetod valitakse programmi kompileerimise ajal d
//
//Paaride" võti-väärtus" salvesatamiseks sobib Java APIs klass:
//c. HashMap
//
//for(eeltegevus; jätkamistingimus; järeltegevused){
//avaldis;
//}