package praktikum6;

import lib.TextIO;

public class ArvamisM2ng {

	public static void main(String[] args) {
		int arvutiArv = suvalineArv(1, 100);

		while (true) {
			System.out.println("Arva ära, mis arv on");
			int kasutajaArv = TextIO.getlnInt();

			if (kasutajaArv == arvutiArv) {
				System.out.println("Arvasid ära!");
				break;
			} else if (kasutajaArv > arvutiArv) {
				System.out.println("Liiga suur");
			} else {
				System.out.println("Liiga väike");
			}

		}
	}

	public static int suvalineArv(int min, int max) {
		int vahemik = max - min + 1;
		return (int) ((Math.random() * vahemik) + min);

	}

}
