package praktikum6;

import praktikum1.TextIO;

public class KullJaKiri {

	public static void main(String[] args) {

		int m2ngijaRaha = 100;

		while (m2ngijaRaha > 0) {
			System.out.println("Sinu raha:" + m2ngijaRaha);

			System.out.println("Sisesta panuse suurus.(Max:25)");
			int maxPanus = Math.min(m2ngijaRaha, 25);
			// int panus = Meetodid.kasutajaSisestus(1, 25);
			// Peaks olema meetod eelnevalt olemas.
			int panus = TextIO.getlnInt();
			m2ngijaRaha -= panus;

			int myndiVise = ArvamisM2ng.suvalineArv(0, 1);

			if (myndiVise == 1) {
				System.out.println("Tuli kiri, saad topelt raha tagasi.");
				m2ngijaRaha += panus * 2;
			} else {
				System.out.println("Tuli kull, ei saa midagi tagasi.");
			}

		}

	}

}
