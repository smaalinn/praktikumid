package praktikum6;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ArvudTagurpidi {
	public static void main(String[] args) {

		int[] nimed = new int[10];
		Scanner sc = new Scanner(System.in);
		int randomNum = (int) (Math.random() * 10);

		System.out.println("Sisesta 10 arvu: ");

		for (int i = 0; i < 10; i++) {
			System.out.print(i + 1 + ") ");
			try {
				nimed[i] = sc.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("ERROR " + e);
			}
			;
		}

		System.out.println("Valituks osutus: " + nimed[randomNum]);

		sc.close();
	}
}