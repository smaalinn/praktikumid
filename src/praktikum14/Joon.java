package praktikum14;

public class Joon {
	
	Punkt alguspunkt, l6pppunkt;

	public Joon(Punkt alguspunkt, Punkt l6pppunkt) {
		this.alguspunkt = alguspunkt;
		this.l6pppunkt = l6pppunkt;
	}

	@Override
	public String toString() {
		return "Joon(" + alguspunkt + ", " + l6pppunkt + ")";
	}
	
	public double pikkus() {
		double a = l6pppunkt.getY() - alguspunkt.getY();
		double b = l6pppunkt.getX() - alguspunkt.getX();
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}
	
	public static void staatilineMeetod() {
		System.out.println("Olen suht mõttetu staatiline meetod.");
	}
}