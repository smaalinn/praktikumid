package praktikum14;

public class Katsetused {
	
	public static void main(String[] args) {
		
		Punkt minuPunkt = new Punkt(100, 200);
		Punkt veelYksPunkt = new Punkt(200, 300);
		
		System.out.println(minuPunkt);
		System.out.println(veelYksPunkt);
		
		Joon minuJoon = new Joon(minuPunkt, veelYksPunkt);
		System.out.println(minuJoon);
		System.out.println("Joone pikkus on: " + minuJoon.pikkus());
		
		Joon.staatilineMeetod();
		
		Ring minuRing = new Ring(minuPunkt, 50);
		System.out.println("Ringi ümbermõõt: " + minuRing.ymberm66t());
		System.out.println("Ringi pindala: " + minuRing.pindala());
		
		Silinder minuSilinder = new Silinder(minuRing, 100);
		System.out.println(minuSilinder);
		
		
	}

}