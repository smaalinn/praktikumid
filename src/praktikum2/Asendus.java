package praktikum2;

import lib.TextIO;

public class Asendus {

	public static void main(String[] args) {

		String lugu = "";
		System.out.println("Kirjuta mingi stoori: ");
		lugu = TextIO.getln();
		System.out.printf(lugu.replace("a", "_"));
	}

}
