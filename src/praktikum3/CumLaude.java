package praktikum3;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

		while (true) {
		System.out.println("Palun sisesta keskmine hinne");
		double keskmineHinne = TextIO.getDouble();

		if (keskmineHinne < 0 || keskmineHinne > 5) {
			System.out.println("Vigane hinne");
			return;
		}

		System.out.println("Palun sisesta lõputöö hinne");
		int l6put66 = TextIO.getInt();

		if (l6put66 < 0 || l6put66 > 5) {
			System.out.println("Vigane hinne");
			return;
		}

		if (keskmineHinne > 4.5) {
			if (l6put66 == 5)
				System.out.println("Jah, Sa saad Cum Laude!");
			return;

		} else {
			System.out.println("Ei saa.");
		}

	}

}
}
