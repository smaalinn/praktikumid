package praktikum10;

public class Fibonacci {
	public static void main(String[] args) {
		int i = 0;
		long fibValue = 0;

		while (fibValue >= 0) {

			if (fibValue >= 0) {
				System.out.println(fibValue);
				i++;
			}

		}

		System.out.println("Final: " + i);

	}

	public static int fibonacci(int n) {
		if (n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return fibonacci(n - 1) + fibonacci(n - 2);
		}
	}

}
