package praktikum10;

import java.util.Arrays;
import java.io.File;

public class FailiNimekiri {

	public static void trykiFailid(int subTree, String kataloogiTee) {

		File kataloog = new File(kataloogiTee);
		File[] failid = kataloog.listFiles();
		Arrays.sort(failid);

		for (File file : failid) {
			if (file.isDirectory()) {
				// add tabs
				for (int i = 0; i < subTree; i++) {
					System.out.print("   ");
				}

				System.out.print("Folder: " + file.getAbsolutePath() + "\n");
				// rekursiooni käivitus:
				trykiFailid(subTree + 1, file.getAbsolutePath());
			} else {
				// add tabs
				for (int i = 0; i < subTree; i++) {
					System.out.print("   ");
				}

				System.out.print("File: ");
				System.out.println(file.getAbsoluteFile());
			}

		}
	}

	public static void main(String[] args) {
		// muuda direcotory-t vastavalt enda arvutile.
		trykiFailid(0, "/home/kasutajanimi/Documents/workspace/Praktikumid/src");
	}

}